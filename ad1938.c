//
// Created by Csurleny on 7/11/2022.
//

#include "ad1938.h"
#include "main.h"
#include <stdint.h>

#define CONFIG_READ_AD1938_REG  1

extern SPI_HandleTypeDef hspi2;
#define codec_hspi hspi2

static inline void set_cs(){
    HAL_GPIO_WritePin(nCS_C_GPIO_Port, nCS_C_Pin, GPIO_PIN_SET);
}

static inline void reset_cs(){
    HAL_GPIO_WritePin(nCS_C_GPIO_Port, nCS_C_Pin, GPIO_PIN_RESET);
}

static inline void set_rst(){
    HAL_GPIO_WritePin(RST_C_GPIO_Port, RST_C_Pin, GPIO_PIN_SET);
}

static inline void reset_rst(){
    HAL_GPIO_WritePin(RST_C_GPIO_Port, RST_C_Pin, GPIO_PIN_RESET);
}

#if CONFIG_READ_AD1938_REG == 1
uint8_t ReceiveBuffer[16];
#endif

const uint8_t ConfigParams1938[] = {
        AD1938_CODEC_ADDR, CLKCTRL0, DIS_ADC_DAC | INPUT256 | PLL_IN_MCLK | MCLK_OUT_XTAL | PLL_IN_MCLK,
        AD1938_CODEC_ADDR, CLKCTRL1, DAC_CLK_PLL | ADC_CLK_PLL | ENA_VREF,
        AD1938_CODEC_ADDR, DACMUTE, 0xFF,

        AD1938_CODEC_ADDR, DACCTRL1,DAC_LRCLK_POL_INV | DAC_BCLK_POL_INV | DAC_CHANNELS_8 | DAC_LATCH_MID | DAC_BCLK_SLAVE | DAC_LRCLK_SLAVE,
        AD1938_CODEC_ADDR, DACCTRL2, DAC_WIDTH_24 | DAC_OUT_POL_NORM | DAC_UNMUTE_ALL,
        AD1938_CODEC_ADDR, DACCTRL0, DAC_FMT_TDM | DAC_BCLK_DLY_0 | DAC_SR_48k,

        AD1938_CODEC_ADDR, ADCCTRL0, ADC_SR_48K,
        AD1938_CODEC_ADDR, ADCCTRL1, ADC_LATCH_MID | ADC_FMT_TDM | ADC_WIDTH_24 | ADC_BCLK_DLY_0,
        AD1938_CODEC_ADDR, ADCCTRL2,ADC_BCLK_SRC_INT | ADC_BCLK_MASTER | ADC_CHANNELS_8 | ADC_LRCLK_MASTER | ADC_LRCLK_POL_INV | ADC_BCLK_POL_INV | ADC_LRCLK_FMT_50_50,

        AD1938_CODEC_ADDR, DACVOL_L1, DACVOL_MAX,
        AD1938_CODEC_ADDR, DACVOL_R1, DACVOL_MAX,
        AD1938_CODEC_ADDR, DACVOL_L2, DACVOL_MAX,
        AD1938_CODEC_ADDR, DACVOL_R2, DACVOL_MAX,
        AD1938_CODEC_ADDR, DACVOL_L3, DACVOL_MAX,
        AD1938_CODEC_ADDR, DACVOL_R3, DACVOL_MAX,
        AD1938_CODEC_ADDR, DACVOL_L4, DACVOL_MAX,
        AD1938_CODEC_ADDR, DACVOL_R4, DACVOL_MAX,

        AD1938_CODEC_ADDR, CLKCTRL0, ENA_ADC_DAC | PLL_IN_MCLK | MCLK_OUT_XTAL | INPUT256 | PLL_PWR_UP,
        AD1938_CODEC_ADDR, DACMUTE, 0x00
};

static uint8_t byteBuffer[3];

static inline void AD1938_writeReg(uint8_t rwaddr, uint8_t regaddr, uint8_t regdata){
    byteBuffer[0] = rwaddr << 1;
    byteBuffer[1] = regaddr;
    byteBuffer[2] = regdata;
    reset_cs();
    HAL_SPI_Transmit(&codec_hspi, byteBuffer, 3, HAL_MAX_DELAY);
    set_cs();
}

static inline uint8_t AD1938_readReg(uint8_t rwaddr, uint8_t regaddr){
    uint8_t result;
    byteBuffer[0] = (rwaddr << 1) | 1;
    byteBuffer[1] = regaddr;
    reset_cs();
    HAL_SPI_Transmit(&codec_hspi, byteBuffer, 2, HAL_MAX_DELAY);
    HAL_SPI_Receive(&codec_hspi, &result, 1, HAL_MAX_DELAY);
    set_cs();
    return result;
}

void AD1938_init(){
    uint32_t configSize;

    // Insert a CODEC reset
    reset_rst();
    HAL_Delay(200);
    set_rst();
    HAL_Delay(1);

    // Configure The CODEC
    configSize = sizeof(ConfigParams1938);
    for(uint32_t i = 0; i < configSize; i+=3){
        AD1938_writeReg(ConfigParams1938[i], ConfigParams1938[i+1], ConfigParams1938[i+2]);
        HAL_Delay(10);
    }
#if(CONFIG_READ_AD1938_REG == 1)
    HAL_Delay(200);
    for(uint32_t i = 0; i < 16; i++){
        ReceiveBuffer[i] = AD1938_readReg(ConfigParams1938[i], i);
        HAL_Delay(1);
    }
#endif
}
